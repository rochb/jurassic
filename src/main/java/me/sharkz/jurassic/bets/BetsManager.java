package me.sharkz.jurassic.bets;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassic.settings.SettingsManager;
import me.sharkz.jurassic.stats.PlayerStats;
import me.sharkz.jurassic.stats.StatsManager;
import me.sharkz.jurassic.storage.JStorage;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.hooks.VaultHook;
import me.sharkz.milkalib.utils.MilkaUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * @author Roch Blondiaux
 */
public class BetsManager extends MilkaUtils {

    private final List<Bet> bets = new ArrayList<>();
    private final JStorage storage;
    private final StatsManager statsManager;
    private final SettingsManager settingsManager;

    public BetsManager(JStorage storage, StatsManager statsManager, SettingsManager settingsManager) {
        this.storage = storage;
        this.statsManager = statsManager;
        this.settingsManager = settingsManager;
        this.bets.addAll(storage.get());
    }

    public void delete(Bet bet){
        storage.delete(bet);
        bets.remove(bet);
    }

    public Bet create(Player player, double amount) {
        return new Bet(player, amount);
    }

    public void makeChoice(Bet bet, BetOption option) {
        bet.setOption(option);
        VaultHook.getEconomy().withdrawPlayer(bet.getPlayer(), bet.getAmount());
        bets.add(bet);
        storage.insert(bet);
    }

    public void play(Player player, Bet bet, BetOption option) {
        if (VaultHook.getEconomy() != null) VaultHook.getEconomy().withdrawPlayer(player, bet.getAmount());

        if (option.equals(bet.getOption())) {
            statsManager.getStats(player)
                    .ifPresent(stats -> {
                        stats.addPar();
                        statsManager.update(stats);
                    });
            statsManager.getStats(bet.getPlayer())
                    .ifPresent(stats -> {
                        stats.addPar();
                        statsManager.update(stats);
                    });
            if (settingsManager.areNotificationsEnabled(player))
                message(player, JcEn.EQUALITY.name(), JcEn.EQUALITY.toString());
            if (bet.getPlayer().isOnline() && settingsManager.areNotificationsEnabled(bet.getPlayer()))
                message(bet.getPlayer().getPlayer(), JcEn.EQUALITY.name(), JcEn.EQUALITY.toString());

        } else if ((option.equals(BetOption.ROCK) && bet.getOption().equals(BetOption.SCISSORS))
                || (option.equals(BetOption.PAPER) && bet.getOption().equals(BetOption.ROCK))
                || (option.equals(BetOption.SCISSORS)) && bet.getOption().equals(BetOption.PAPER)) {
            statsManager.getStats(player)
                    .ifPresent(stats -> {
                        stats.addWin();
                        statsManager.update(stats);
                    });
            statsManager.getStats(bet.getPlayer())
                    .ifPresent(stats -> {
                        stats.addLose();
                        statsManager.update(stats);
                    });

            if (settingsManager.areNotificationsEnabled(player))
                message(player, JcEn.WIN.name(), JcEn.WIN.toString(), ImmutableMap.of("%amount%", String.valueOf(bet.getAmount() * 2)));
            if (bet.getPlayer().isOnline() && settingsManager.areNotificationsEnabled(bet.getPlayer()))
                message(bet.getPlayer().getPlayer(), JcEn.LOSE.name(), JcEn.LOSE.toString());

            Bukkit.getOnlinePlayers()
                    .stream()
                    .filter(settingsManager::areNotificationsEnabled)
                    .forEach(player1 -> message(player1, JcEn.BROADCAST.name(), JcEn.BROADCAST.toString(), getPlaceholders(bet, player, bet.getPlayer())));
            if (VaultHook.getEconomy() != null) VaultHook.getEconomy().depositPlayer(player, bet.getAmount() * 2);
        } else {
            statsManager.getStats(player)
                    .ifPresent(stats -> {
                        stats.addLose();
                        statsManager.update(stats);
                    });
            statsManager.getStats(bet.getPlayer())
                    .ifPresent(stats -> {
                        stats.addWin();
                        statsManager.update(stats);
                    });

            if (bet.getPlayer().isOnline() && settingsManager.areNotificationsEnabled(bet.getPlayer()))
                message(bet.getPlayer().getPlayer(), JcEn.WIN.name(), JcEn.WIN.toString(), ImmutableMap.of("%amount%", String.valueOf(bet.getAmount() * 2)));
            if (settingsManager.areNotificationsEnabled(player))
                message(player, JcEn.LOSE.name(), JcEn.LOSE.toString());
            Bukkit.getOnlinePlayers()
                    .stream()
                    .filter(settingsManager::areNotificationsEnabled)
                    .forEach(player1 -> message(player1, JcEn.BROADCAST.name(), JcEn.BROADCAST.toString(), getPlaceholders(bet, bet.getPlayer(), player)));
            if (VaultHook.getEconomy() != null)
                VaultHook.getEconomy().depositPlayer(bet.getPlayer(), bet.getAmount() * 2);
        }
        player.closeInventory();
        storage.delete(bet);
        bets.remove(bet);
    }

    private Map<String, String> getPlaceholders(Bet bet, OfflinePlayer winner, OfflinePlayer looser) {
        Map<String, String> p = new HashMap<>(getStats(winner, looser));
        p.put("%winner%", winner.getName());
        p.put("%loser%", looser.getName());
        p.put("%amount%", String.valueOf(bet.getAmount() * 2));
        return p;
    }

    private Map<String, String> getStats(OfflinePlayer winner, OfflinePlayer looser) {
        Map<String, String> p = new HashMap<>();
        p.putAll(getStatsPlaceholders(winner, false));
        p.putAll(getStatsPlaceholders(looser, true));
        return p;
    }

    private Map<String, String> getStatsPlaceholders(OfflinePlayer player, boolean looser) {
        String winKey = "%win%";
        String loseKey = "%lose%";
        String parKey = "%par%";
        if (looser) {
            winKey = "%win_loser%";
            loseKey = "%lose_loser%";
            parKey = "%par_loser%";
        }
        if (!statsManager.getStats(player).isPresent()) return ImmutableMap.of(winKey, "0", loseKey, "0", parKey, "0");
        PlayerStats ps = statsManager.getStats(player).get();
        return ImmutableMap.of(winKey, String.valueOf(ps.getWin()), loseKey, String.valueOf(ps.getLose()), parKey, String.valueOf(ps.getPar()));
    }

    public boolean hasBet(OfflinePlayer player){
        return bets.stream().anyMatch(bet -> bet.getPlayer().getUniqueId().equals(player.getUniqueId()));
    }

    public Optional<Bet> getPlayerBet(OfflinePlayer player){
        return bets.stream().filter(bet -> bet.getPlayer().getUniqueId().equals(player.getUniqueId())).findFirst();
    }


    public void update(Bet bet){
        storage.update(bet);
    }

    public List<Bet> getBets() {
        return bets;
    }

    public SettingsManager getSettingsManager() {
        return settingsManager;
    }
}
