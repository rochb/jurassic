package me.sharkz.jurassic;

import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.commands.MainCommand;
import me.sharkz.jurassic.configurations.Configuration;
import me.sharkz.jurassic.listeners.PlayersListener;
import me.sharkz.jurassic.settings.SettingsManager;
import me.sharkz.jurassic.stats.StatsManager;
import me.sharkz.jurassic.storage.JStorage;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.jurassic.ui.MainUI;
import me.sharkz.jurassic.ui.SUI;
import me.sharkz.jurassic.ui.SelectionUI;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.hooks.VaultHook;

import java.util.Objects;

public final class Jurassic extends MilkaPlugin {

    private Configuration config;

    private BetsManager betsManager;
    private JStorage storage;

    @Override
    public void onEnable() {
        /* Initialize */
        init(this);

        /* Header */
        sendHeader("Sharkz");

        /* Dependencies */
        initDependencies();

        /* Hooks */
        initHooks();
        registerHook(new VaultHook());
        registerHook(new PapiHook());

        /* Translations */
        registerTranslation(JcEn.class);

        /* Configurations */
        initConfigurations();
        config = (Configuration) registerConfig(new Configuration(this));

        /* Storage */
        storage = new JStorage(config);

        /* Stats */
        StatsManager statsManager = new StatsManager(storage);

        /* Settings */
        SettingsManager settingsManager = new SettingsManager(storage);

        /* Bets */
        betsManager = new BetsManager(storage, statsManager, settingsManager);

        /* Commands */
        initCommands();
        registerCommand("rps", new MainCommand(this, betsManager));

        /* UI */
        initInventories();
        registerInventory(1, new MainUI());
        registerInventory(2, new SelectionUI());
        registerInventory(3, new SUI());

        /* Listeners */
        registerListener(new PlayersListener(statsManager, settingsManager));

        /* Footer */
        sendFooter();
    }

    @Override
    public void onDisable() {
        Objects.requireNonNull(storage).getDatabase().closeConnection();
    }

    @Override
    protected boolean isPaid() {
        return true;
    }

    @Override
    protected int getConfigurationVersion() {
        return 1;
    }

    @Override
    public String getPrefix() {
        return "&b&lJurassic &f|";
    }

    @Override
    public String getLanguage() {
        return getConfig().getString("language", "en_US");
    }

    public BetsManager getBetsManager() {
        return betsManager;
    }

    public Configuration getConfiguration() {
        return config;
    }
}
