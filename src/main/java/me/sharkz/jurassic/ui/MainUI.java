package me.sharkz.jurassic.ui;

import me.sharkz.jurassic.Jurassic;
import me.sharkz.jurassic.bets.Bet;
import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.settings.SettingsManager;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.hooks.VaultHook;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.Pagination;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import me.sharkz.milkalib.utils.items.SkullCreator;
import me.sharkz.milkalib.utils.xseries.XMaterial;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 */
public class MainUI extends MilkaInventory {

    private final BetsManager manager;
    private final SettingsManager settingsManager;
    private BukkitTask task;

    private static final int[] graySlots = {0, 9, 18, 27, 36, 8, 17, 26, 35, 44};
    private static final int[] whiteSlots = {1, 2, 3, 4, 5, 6, 7, 10, 19, 12, 14, 16, 19, 20, 21, 22, 23, 24, 25};
    private static final int[] betsSlots = {28, 29, 30, 31, 32, 33, 34, 37, 38, 39, 40, 41, 42, 43, 46, 47, 48, 49, 50, 51, 52};

    public MainUI() {
        manager = Jurassic.getPlugin(Jurassic.class).getBetsManager();
        settingsManager = manager.getSettingsManager();

        createInventory("&c&lERROR", 9);
    }

    @Override
    public InventoryResult open(Player player, Object... objects) {
        createInventory(ChatColor.AQUA + "Bets", 54);

        /* Get bet */
        int page = 1;
        if (objects != null && objects.length == 1 && objects[0] instanceof Integer)
            page = (int) objects[0];

        /* Play items */
        addItem(11, new ItemBuilder(Objects.requireNonNull(XMaterial.CLAY_BALL.parseItem())).setName("&fRock"));

        addItem(13, new ItemBuilder(Objects.requireNonNull(XMaterial.PAPER.parseItem())).setName("&fPaper"));

        addItem(15, new ItemBuilder(Objects.requireNonNull(XMaterial.SHEARS.parseItem())).setName("&fScissors"));

        /* Fillers */
        for (int graySlot : graySlots)
            addItem(graySlot, new ItemBuilder(Objects.requireNonNull(XMaterial.GRAY_STAINED_GLASS_PANE.parseItem())).setName("&r"));

        for (int slot : whiteSlots)
            addItem(slot, new ItemBuilder(Objects.requireNonNull(XMaterial.WHITE_STAINED_GLASS_PANE.parseItem())).setName("&r"));

        int finalPage = page;
        task = new BukkitRunnable() {
            @Override
            public void run() {
                display(finalPage, player);
            }
        }.runTaskTimer(Jurassic.getPlugin(Jurassic.class), 0L, 20L);

        return InventoryResult.SUCCESS;
    }

    private void display(int page, Player player) {
        Pagination<Bet> pagination = new Pagination<>();
        AtomicInteger slot = new AtomicInteger(0);

        for (int betsSlot : betsSlots)
            getInventory().setItem(betsSlot, null);

        pagination.paginate(manager.getBets(), betsSlots.length, page)
                .forEach(bet -> addItem(betsSlots[slot.getAndIncrement()], new ItemBuilder(SkullCreator.itemFromName(bet.getPlayer().getName()))
                        .setName(ChatColor.AQUA + bet.getPlayer().getName())
                        .setLore("&r",
                                "&7Amount bet &b&l" + bet.getAmount(),
                                "&r",
                                "&b&lClick&7 to play."))
                        .setClick(e -> {
                            if (player.getUniqueId().equals(bet.getPlayer().getUniqueId())) {
                                message(player, JcEn.CANNOT_JOIN_YOURSELF.name(), JcEn.CANNOT_JOIN_YOURSELF.toString());
                                return;
                            }
                            if (VaultHook.getEconomy() == null || !VaultHook.getEconomy().has(player, bet.getAmount())) {
                                message(player, JcEn.NOT_ENOUGH_MONEY.name(), JcEn.NOT_ENOUGH_MONEY.toString());
                                return;
                            }
                            openInventory(Jurassic.getPlugin(Jurassic.class), player, 3, bet);
                        }));

        /* Utils */
        addItem(45, new ItemBuilder(Objects.requireNonNull(XMaterial.FEATHER.parseItem())).setName("&aNotification"))
                .setClick(e -> {
                    settingsManager.setNotificationsEnabled(player, !settingsManager.areNotificationsEnabled(player));
                    if (settingsManager.areNotificationsEnabled(player))
                        message(player, JcEn.NOTIFICATIONS_ENABLED.name(), JcEn.NOTIFICATIONS_ENABLED.toString());
                    else
                        message(player, JcEn.NOTIFICATIONS_DISABLED.name(), JcEn.NOTIFICATIONS_DISABLED.toString());
                });

        if (getMaxPage(manager.getBets(), betsSlots.length) < page)
            addItem(53, new ItemBuilder(Objects.requireNonNull(XMaterial.ARROW.parseItem())).setName("&fNext"))
                    .setClick(e -> display(page + 1, player));
        else
            addItem(53, new ItemBuilder(Objects.requireNonNull(XMaterial.BARRIER.parseItem())).setName("&r"));
    }

    @Override
    public void onClose(InventoryCloseEvent e, Player player) {
        if (task != null) task.cancel();
    }
}
