package me.sharkz.jurassic.settings;

import me.sharkz.jurassic.storage.JStorage;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author Roch Blondiaux
 */
public class SettingsManager {

    private final List<PlayerSettings> settings = new ArrayList<>();
    private final JStorage storage;

    public SettingsManager(JStorage storage) {
        this.storage = storage;
        this.settings.addAll(storage.getSettings_());
    }

    public void init(OfflinePlayer player) {
        if (get(player).isPresent()) return;
        PlayerSettings s = new PlayerSettings(player);
        settings.add(s);
        storage.insert(s);
    }

    public Optional<PlayerSettings> get(OfflinePlayer player) {
        return settings.stream().filter(playerSettings -> playerSettings.getPlayer().equals(player)).findFirst();
    }

    public boolean areNotificationsEnabled(OfflinePlayer player) {
        AtomicBoolean a = new AtomicBoolean(true);
        get(player)
                .ifPresent(playerSettings -> a.set(playerSettings.areNotificationsEnabled()));
        return a.get();
    }

    public void setNotificationsEnabled(OfflinePlayer player, boolean enabled) {
        get(player)
                .ifPresent(playerSettings -> {
                    playerSettings.setNotificationsEnabled(enabled);
                    storage.update(playerSettings);
                });
    }

    public List<PlayerSettings> getSettings() {
        return settings;
    }
}
