package me.sharkz.jurassic.bets;

import org.bukkit.OfflinePlayer;

import java.util.UUID;

/**
 * @author Roch Blondiaux
 */
public class Bet {

    private final UUID id;
    private final OfflinePlayer player;
    private double amount;
    private BetOption option;

    public Bet(OfflinePlayer player, double amount) {
        this.id = UUID.randomUUID();
        this.player = player;
        this.amount = amount;
        this.option = BetOption.ROCK;
    }

    public Bet(UUID id, OfflinePlayer player, double amount, BetOption option) {
        this.id = id;
        this.player = player;
        this.amount = amount;
        this.option = option;
    }

    public UUID getId() {
        return id;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public double getAmount() {
        return amount;
    }

    public BetOption getOption() {
        return option;
    }

    public void setOption(BetOption option) {
        this.option = option;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
