package me.sharkz.jurassic.stats;

import org.bukkit.OfflinePlayer;

/**
 * @author Roch Blondiaux
 */
public class PlayerStats {

    private final OfflinePlayer player;
    private int lose;
    private int win;
    private int par;

    public PlayerStats(OfflinePlayer player) {
        this.player = player;
        this.lose = 0;
        this.win = 0;
        this.par = 0;
    }

    public PlayerStats(OfflinePlayer player, int lose, int win, int par) {
        this.player = player;
        this.lose = lose;
        this.win = win;
        this.par = par;
    }

    public void addLose() {
        this.lose++;
    }

    public void addWin() {
        this.win++;
    }

    public void addPar() {
        this.par++;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public int getLose() {
        return lose;
    }

    public int getWin() {
        return win;
    }

    public int getPar() {
        return par;
    }
}
