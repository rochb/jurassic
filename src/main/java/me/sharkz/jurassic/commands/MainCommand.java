package me.sharkz.jurassic.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassic.Jurassic;
import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.commands.defaults.HelpCommand;
import me.sharkz.milkalib.commands.defaults.ReloadCommand;
import me.sharkz.milkalib.commands.defaults.VersionCommand;
import me.sharkz.milkalib.hooks.VaultHook;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;

/**
 * @author Roch Blondiaux
 */
public class MainCommand extends MilkaCommand {

    private final Jurassic plugin;
    private final BetsManager manager;

    public MainCommand(Jurassic plugin, BetsManager manager) {
        this.plugin = plugin;
        this.manager = manager;

        setDescription("Main command of the plugin.");
        setConsoleCanUse(false);

        /* Subs */
        addSubCommand(new ReloadCommand(plugin, "jurassic.reload"));
        addSubCommand(new VersionCommand());
        addSubCommand(new HelpCommand(plugin));
        addSubCommand(new CancelCommand(manager));

        /* Args */
        addOptionalArg("bet");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        if (args.length == 1) {
            /* Amount bet */
            if (!StringUtils.isNumeric(argAsString(0)) || argAsDouble(0) < 0) {
                message(player, JcEn.INVALID_AMOUNT.name(), JcEn.INVALID_AMOUNT.toString());
                return CommandResult.COMPLETED;
            }

            /* Vault */
            if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
                MilkaLogger.error("Please install Vault !");
                return CommandResult.ERROR;
            }

            /* Economy */
            if (!VaultHook.getEconomy().has(player, argAsDouble(0))) {
                message(player, JcEn.NOT_ENOUGH_MONEY.name(), JcEn.NOT_ENOUGH_MONEY.toString());
                return CommandResult.COMPLETED;
            }

            if(manager.hasBet(player)){
                message(player, JcEn.ALREADY_BET.name(), JcEn.ALREADY_BET.toString());
                return CommandResult.COMPLETED;
            }

            openInventory(Jurassic.getPlugin(Jurassic.class), player, 2, manager.create(player, argAsDouble(0)));
            return CommandResult.COMPLETED;
        }

        openInventory(plugin, player, 1);
        return CommandResult.COMPLETED;
    }
}
