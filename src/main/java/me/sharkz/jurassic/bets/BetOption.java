package me.sharkz.jurassic.bets;

import org.bukkit.Material;

/**
 * @author Roch Blondiaux
 */
public enum BetOption {
    PAPER(Material.PAPER),
    ROCK(Material.CLAY_BALL),
    SCISSORS(Material.SHEARS);

    private final Material icon;

    BetOption(Material icon) {
        this.icon = icon;
    }

    public Material getIcon() {
        return icon;
    }
}
