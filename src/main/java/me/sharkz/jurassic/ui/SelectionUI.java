package me.sharkz.jurassic.ui;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassic.Jurassic;
import me.sharkz.jurassic.bets.Bet;
import me.sharkz.jurassic.bets.BetOption;
import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import me.sharkz.milkalib.utils.xseries.XMaterial;
import org.bukkit.entity.Player;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 */
public class SelectionUI extends MilkaInventory {

    private final BetsManager manager;

    public SelectionUI() {
        this.manager = Jurassic.getPlugin(Jurassic.class).getBetsManager();

        createInventory("&fMake your choice!", 9);
    }

    @Override
    public InventoryResult open(Player player, Object... objects) {
        if (objects == null
                || objects.length != 1
                || !(objects[0] instanceof Bet)) return InventoryResult.ERROR;
        Bet bet = (Bet) objects[0];

        /* Play items */
        addItem(2, new ItemBuilder(Objects.requireNonNull(XMaterial.CLAY_BALL.parseItem())).setName("&fRock"))
                .setClick(e -> {
                    manager.makeChoice(bet, BetOption.ROCK);
                    player.closeInventory();
                    message(player, JcEn.BET_CREATED.name(), JcEn.BET_CREATED.toString());
                    message(player, JcEn.BET_INFO.name(), JcEn.BET_INFO.toString(), ImmutableMap.of("%amount%", String.valueOf(bet.getAmount())));
                });

        addItem(4, new ItemBuilder(Objects.requireNonNull(XMaterial.PAPER.parseItem())).setName("&fPaper"))
                .setClick(e -> {
                    manager.makeChoice(bet, BetOption.PAPER);
                    player.closeInventory();
                    message(player, JcEn.BET_CREATED.name(), JcEn.BET_CREATED.toString());
                    message(player, JcEn.BET_INFO.name(), JcEn.BET_INFO.toString(), ImmutableMap.of("%amount%", String.valueOf(bet.getAmount())));
                });

        addItem(6, new ItemBuilder(Objects.requireNonNull(XMaterial.SHEARS.parseItem())).setName("&fScissors"))
                .setClick(e -> {
                    manager.makeChoice(bet, BetOption.SCISSORS);
                    player.closeInventory();
                    message(player, JcEn.BET_CREATED.name(), JcEn.BET_CREATED.toString());
                    message(player, JcEn.BET_INFO.name(), JcEn.BET_INFO.toString(), ImmutableMap.of("%amount%", String.valueOf(bet.getAmount())));
                });

        addFillers(new ItemBuilder(Objects.requireNonNull(XMaterial.WHITE_STAINED_GLASS_PANE.parseItem())).setName("&r"));
        return InventoryResult.SUCCESS;
    }
}
