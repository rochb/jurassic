package me.sharkz.jurassic.storage;

import me.sharkz.jurassic.bets.Bet;
import me.sharkz.jurassic.bets.BetOption;
import me.sharkz.jurassic.configurations.Configuration;
import me.sharkz.jurassic.settings.PlayerSettings;
import me.sharkz.jurassic.stats.PlayerStats;
import me.sharkz.milkalib.storage.StorageManager;
import me.sharkz.milkalib.utils.C;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.milkalib.utils.sql.Column;
import me.sharkz.milkalib.utils.sql.DataTypes;
import me.sharkz.milkalib.utils.sql.QueryBuilder;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 */
public class JStorage extends StorageManager {

    private String BETS_TABLE;
    private String STATS_TABLE;
    private String SETTINGS_TABLE;

    public JStorage(Configuration config) {
        super(config.getStorage());
    }

    @Override
    protected void createDefaultTables() {
        BETS_TABLE = getTableName("bets");
        STATS_TABLE = getTableName("stats");
        SETTINGS_TABLE = getTableName("settings");

        getDatabase().query(new QueryBuilder(STATS_TABLE).createTableIfNotExists()
                .column(Column.dataType("player", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("win", DataTypes.INT))
                .column(Column.dataType("lost", DataTypes.INT))
                .column(Column.dataType("par", DataTypes.INT))
                .primaryKey("player")
                .build());

        getDatabase().query(new QueryBuilder(BETS_TABLE).createTableIfNotExists()
                .column(Column.dataType("id", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("player", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("amount", DataTypes.DOUBLE))
                .column(Column.dataType("choice", DataTypes.Limit.VARCHAR, 50))
                .primaryKey("id")
                .build());

        getDatabase().query(new QueryBuilder(SETTINGS_TABLE).createTableIfNotExists()
                .column(Column.dataType("player", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("notifications", DataTypes.BOOLEAN))
                .primaryKey("player")
                .build());
    }

    public void insert(PlayerSettings settings) {
        getDatabase().asyncQuery(new QueryBuilder(SETTINGS_TABLE).insert()
                .insert("player", settings.getPlayer().getUniqueId().toString())
                .insert("notifications", settings.areNotificationsEnabled())
                .build());
    }

    public void update(PlayerSettings settings) {
        getDatabase().asyncQuery(new QueryBuilder(SETTINGS_TABLE).update()
                .set("notifications", settings.areNotificationsEnabled())
                .toWhere()
                .where("player", settings.getPlayer().getUniqueId().toString())
                .build());
    }

    public List<PlayerSettings> getSettings_() {
        List<PlayerSettings> settings = new ArrayList<>();
        ResultSet rs = getDatabase().executeQuery(new QueryBuilder(SETTINGS_TABLE).select().buildAllColumns());
        try {
            while (rs.next()) {
                OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player")));
                settings.add(new PlayerSettings(player, rs.getBoolean("notifications")));
            }
        } catch (SQLException e) {
            MilkaLogger.error("Couldn't load players settings from database: ");
            e.printStackTrace();
        }
        return settings;
    }

    public void insert(PlayerStats stats) {
        getDatabase().asyncQuery(new QueryBuilder(STATS_TABLE).insert()
                .insert("player", stats.getPlayer().getUniqueId().toString())
                .insert("win", stats.getWin())
                .insert("lost", stats.getLose())
                .insert("par", stats.getPar())
                .build());
    }

    public void update(PlayerStats stats) {
        getDatabase().asyncQuery(new QueryBuilder(STATS_TABLE).update()
                .set("win", stats.getWin())
                .set("lost", stats.getLose())
                .set("par", stats.getPar())
                .toWhere()
                .where("player", stats.getPlayer().getUniqueId().toString())
                .build());
    }

    public List<PlayerStats> getStats() {
        List<PlayerStats> stats = new ArrayList<>();
        ResultSet rs = getDatabase().executeQuery(new QueryBuilder(STATS_TABLE).select().buildAllColumns());
        try {
            while (rs.next()) {
                OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player")));
                stats.add(new PlayerStats(player, rs.getInt("lost"), rs.getInt("win"), rs.getInt("par")));
            }
        } catch (SQLException e) {
            MilkaLogger.error("Couldn't get players stats from database:");
            e.printStackTrace();
        }
        return stats;
    }

    public void insert(Bet bet) {
        getDatabase().asyncQuery(new QueryBuilder(BETS_TABLE).insert()
                .insert("id", bet.getId().toString())
                .insert("player", bet.getPlayer().getUniqueId().toString())
                .insert("amount", bet.getAmount())
                .insert("choice", bet.getOption().name())
                .build());
    }

    public void update(Bet bet) {
        getDatabase().asyncQuery(new QueryBuilder(BETS_TABLE).update()
                .set("choice", bet.getOption().name())
                .set("amount", bet.getAmount())
                .toWhere()
                .where("id", bet.getId().toString())
                .build());
    }

    public void delete(Bet bet) {
        getDatabase().asyncQuery(new QueryBuilder(BETS_TABLE).delete()
                .where("id", bet.getId().toString())
                .build());
    }

    public List<Bet> get() {
        List<Bet> bets = new ArrayList<>();
        ResultSet rs = getDatabase().executeQuery(new QueryBuilder(BETS_TABLE).select().buildAllColumns());
        try {
            while (rs.next()) {
                UUID id = UUID.fromString(rs.getString("id"));
                OfflinePlayer player = Bukkit.getOfflinePlayer(UUID.fromString(rs.getString("player")));
                BetOption option = BetOption.valueOf(rs.getString("choice"));
                bets.add(new Bet(id, player, rs.getDouble("amount"), option));
            }
        } catch (SQLException e) {
            MilkaLogger.error("Couldn't get bets from database: ");
            e.printStackTrace();
        }
        return bets;
    }
}
