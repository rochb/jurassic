package me.sharkz.jurassic.translations;

import me.sharkz.milkalib.translations.ITranslation;
import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum JcEn implements ITranslation {
    INVALID_AMOUNT("&c&l[!] &ePlease specify a valid amount to bet!"),
    NOT_ENOUGH_MONEY("&c&l[!] &eYou don't have enough money to play!"),
    BET_CREATED("&a&l[!] &7You have created a game!"),
    BET_INFO("&7[&c-&7] $&e&l%amount%"),
    BET_AMOUNT_UPDATE("&a&l[!] &7Bet amount has been updated to &a&l%amount%&7!"),
    CANNOT_JOIN_YOURSELF("&c&l[!] &eYou can't join your own game."),
    STARTING("&b&l[!] &7Game will start in &b&l3&7 seconds."),
    WIN("&b&l[!] &7You have &a&lWIN&7 your bet ! You won $&a&l%amount%"),
    LOSE("&b&l[!] &7You have &c&lLOSE&7 your bet!"),
    EQUALITY("&b&l[!] &7Anyone has won this bet!"),
    BROADCAST("&7%winner% &f(W: %win% L: %lose% T: %par%) &ehas &c&lDEFERTED&e the player &7%loser% &f(W: %win_loser% L: %lose_loser% T: %par_loser%) &e in a &6$%amount% &bRPS &ematch."),
    NOTIFICATIONS_ENABLED("&a&l[!] &7You have &a&lenabled&7 the rps notifications."),
    NOTIFICATIONS_DISABLED("&a&l[!] &7You have &d&ldisabled&7 the rps notifications."),
    ALREADY_BET("&c&l[!] &eYou have already bet!"),
    NO_ACTIVE_BET("&c&l[!] &eYou haven't bet yet!"),
    BET_CANCELLED("&a&l[!] &7You have &ccancelled&7 your bet!"),
    ;

    private final String content;
    private TranslationsManager manager;

    JcEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(JcEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

