package me.sharkz.jurassic.configurations;

import me.sharkz.jurassic.Jurassic;
import me.sharkz.milkalib.configuration.Configurable;
import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;

/**
 * @author Roch Blondiaux (Kiwix)
 */
public class Configuration extends ConfigurableFileWrapper {


    @Configurable
    private ConfigurationSection storage;

    public Configuration(Jurassic plugin) {
        super(new File(plugin.getDataFolder(), "config.yml"), plugin.getClass().getClassLoader().getResource("config.yml"));
    }

    public String getUITitle(ConfigurationSection section) {
        return section.getString("title");
    }

    public ConfigurationSection getStorage() {
        return storage;
    }
}