package me.sharkz.jurassic.settings;

import org.bukkit.OfflinePlayer;

/**
 * @author Roch Blondiaux
 */
public class PlayerSettings {

    private final OfflinePlayer player;
    private boolean notifications;

    public PlayerSettings(OfflinePlayer player) {
        this.player = player;
        this.notifications = true;
    }

    public PlayerSettings(OfflinePlayer player, boolean notifications) {
        this.player = player;
        this.notifications = notifications;
    }

    public OfflinePlayer getPlayer() {
        return player;
    }

    public boolean areNotificationsEnabled() {
        return notifications;
    }

    public void setNotificationsEnabled(boolean enabled){
        this.notifications = enabled;
    }
}
