package me.sharkz.jurassic.ui;

import me.sharkz.jurassic.Jurassic;
import me.sharkz.jurassic.bets.Bet;
import me.sharkz.jurassic.bets.BetOption;
import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import me.sharkz.milkalib.utils.xseries.XMaterial;
import org.bukkit.entity.Player;

import java.util.Objects;

/**
 * @author Roch Blondiaux
 */
public class SUI extends MilkaInventory {

    private final BetsManager manager;

    public SUI() {
        this.manager = Jurassic.getPlugin(Jurassic.class).getBetsManager();

        createInventory("&fMake your choice!", 9);
    }

    @Override
    public InventoryResult open(Player player, Object... objects) {
        if (objects == null
                || objects.length == 0
                || !(objects[0] instanceof Bet)) return InventoryResult.ERROR;
        Bet bet = (Bet) objects[0];

        /* Play items */
        addItem(2, new ItemBuilder(Objects.requireNonNull(XMaterial.CLAY_BALL.parseItem())).setName("&fRock"))
                .setClick(e -> manager.play(player, bet, BetOption.ROCK));

        addItem(4, new ItemBuilder(Objects.requireNonNull(XMaterial.PAPER.parseItem())).setName("&fPaper"))
                .setClick(e -> manager.play(player, bet, BetOption.PAPER));

        addItem(6, new ItemBuilder(Objects.requireNonNull(XMaterial.SHEARS.parseItem())).setName("&fScissors"))
                .setClick(e -> manager.play(player, bet, BetOption.SCISSORS));

        addFillers(new ItemBuilder(Objects.requireNonNull(XMaterial.WHITE_STAINED_GLASS_PANE.parseItem())).setName("&r"));
        return InventoryResult.SUCCESS;
    }
}
