package me.sharkz.jurassic.commands;

import me.sharkz.jurassic.bets.Bet;
import me.sharkz.jurassic.bets.BetsManager;
import me.sharkz.jurassic.translations.JcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.hooks.VaultHook;

import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class CancelCommand extends MilkaCommand {

    private final BetsManager manager;

    public CancelCommand(BetsManager manager) {
        this.manager = manager;

        setDescription("Cancel your active bet.");
        addSubCommand("cancel");
        setConsoleCanUse(false);
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<Bet> bBet = manager.getPlayerBet(player);
        if (!bBet.isPresent()) {
            message(player, JcEn.NO_ACTIVE_BET.name(), JcEn.NO_ACTIVE_BET.toString());
            return CommandResult.COMPLETED;
        }
        bBet.ifPresent(bet -> {
            manager.delete(bet);
            message(player, JcEn.BET_CANCELLED.name(), JcEn.BET_CANCELLED.toString());
            if (VaultHook.getEconomy() != null)
                VaultHook.getEconomy().depositPlayer(player, bet.getAmount());
        });
        return CommandResult.COMPLETED;
    }
}
