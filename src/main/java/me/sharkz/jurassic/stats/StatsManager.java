package me.sharkz.jurassic.stats;

import me.sharkz.jurassic.storage.JStorage;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class StatsManager {

    private final List<PlayerStats> stats = new ArrayList<>();
    private final JStorage storage;

    public StatsManager(JStorage storage) {
        this.storage = storage;
        this.stats.addAll(storage.getStats());
    }

    public void init(OfflinePlayer player){
        if(getStats(player).isPresent()) return;
        PlayerStats playerStats = new PlayerStats(player);
        storage.insert(playerStats);
        stats.add(playerStats);
    }

    public Optional<PlayerStats> getStats(OfflinePlayer player){
        return stats.stream().filter(playerStats -> playerStats.getPlayer().equals(player)).findFirst();
    }

    public void update(PlayerStats stats){
        storage.update(stats);
    }
}
