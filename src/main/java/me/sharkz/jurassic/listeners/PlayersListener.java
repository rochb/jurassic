package me.sharkz.jurassic.listeners;

import me.sharkz.jurassic.settings.SettingsManager;
import me.sharkz.jurassic.stats.StatsManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * @author Roch Blondiaux
 */
public class PlayersListener implements Listener {

    private final StatsManager manager;
    private final SettingsManager settingsManager;

    public PlayersListener(StatsManager manager, SettingsManager settingsManager) {
        this.manager = manager;
        this.settingsManager = settingsManager;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        manager.init(e.getPlayer());
        settingsManager.init(e.getPlayer());
    }
}
